GUILE_WARNINGS = -W2
moddir=$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

SOURCES = 					\
  language/cps/hoot.scm				\
  language/cps/hoot/tailify.scm			\
  language/cps/hoot/lower-primcalls.scm		\
  language/cps/hoot/unify-returns.scm		\
  wasm/types.scm				\
  wasm/resolve.scm				\
  wasm/wat.scm					\
  wasm/assemble.scm				\
  wasm/dump.scm					\
  wasm/effects.scm				\
  wasm/link.scm					\
  wasm/lower.scm				\
  wasm/lower-globals.scm			\
  wasm/lower-stringrefs.scm			\
  wasm/optimize.scm				\
  wasm/parse.scm				\
  wasm/canonical-types.scm			\
  wasm/stack.scm				\
  wasm/symbolify.scm				\
  wasm/vm.scm					\
  hoot/backend.scm				\
  hoot/config.scm				\
  hoot/compile.scm				\
  hoot/inline-wasm.scm				\
  hoot/library-group.scm			\
  hoot/primitives.scm				\
  hoot/stdlib.scm				\
  hoot/reflect.scm				\
  hoot/repl.scm					\
  hoot/web-server.scm				\
  scripts/assemble-wasm.scm			\
  scripts/compile-wasm.scm

NOCOMP_SOURCES = 				\
  hoot/assoc.scm				\
  hoot/atomics.scm				\
  hoot/bitvectors.scm				\
  hoot/bitwise.scm				\
  hoot/boxes.scm				\
  hoot/bytevectors.scm				\
  hoot/char.scm					\
  hoot/cond-expand.scm				\
  hoot/control.scm				\
  hoot/debug.scm				\
  hoot/dynamic-wind.scm				\
  hoot/eq.scm					\
  hoot/equal.scm				\
  hoot/error-handling.scm			\
  hoot/errors.scm				\
  hoot/exceptions.scm				\
  hoot/ffi.scm					\
  hoot/fluids.scm				\
  hoot/hashtables.scm				\
  hoot/keywords.scm				\
  hoot/lists.scm				\
  hoot/match.scm				\
  hoot/not.scm					\
  hoot/numbers.scm				\
  hoot/pairs.scm				\
  hoot/parameters.scm				\
  hoot/ports.scm				\
  hoot/procedures.scm				\
  hoot/r7rs-base.scm				\
  hoot/r7rs-case-lambda.scm			\
  hoot/r7rs-char.scm				\
  hoot/r7rs-complex.scm				\
  hoot/r7rs-cxr.scm				\
  hoot/r7rs-eval.scm				\
  hoot/r7rs-file.scm				\
  hoot/r7rs-inexact.scm				\
  hoot/r7rs-lazy.scm				\
  hoot/r7rs-load.scm				\
  hoot/r7rs-process-context.scm			\
  hoot/r7rs-r5rs.scm				\
  hoot/r7rs-read.scm				\
  hoot/r7rs-repl.scm				\
  hoot/r7rs-time.scm				\
  hoot/r7rs-write.scm				\
  hoot/read.scm					\
  hoot/records.scm				\
  hoot/srfi-9.scm				\
  hoot/strings.scm				\
  hoot/symbols.scm				\
  hoot/syntax.scm				\
  hoot/values.scm				\
  hoot/vectors.scm				\
  hoot/write.scm				\
  hoot/generate-char-prelude.scm

GENERATED_NOCOMP_SOURCES = 			\
  hoot/char-prelude.scm

hoot/char-prelude.scm: hoot/generate-char-prelude.scm
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE) $< > "$@"

SUFFIXES = .scm .go
GOBJECTS = $(SOURCES:%.scm=%.go)
CLEANFILES = $(GOBJECTS) $(GENERATED_NOCOMP_SOURCES)
EXTRA_DIST = $(SOURCES) $(NOCOMP_SOURCES)
nobase_mod_DATA = $(SOURCES) $(NOCOMP_SOURCES) $(GENERATED_NOCOMP_SOURCES)
nobase_go_DATA = $(GOBJECTS)

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_goDATA
$(guile_install_go_files): install-nobase_modDATA

.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) compile $(GUILE_WARNINGS) -o "$@" "$<"
